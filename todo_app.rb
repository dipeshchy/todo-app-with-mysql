require 'mysql2'

class TodoApplication

    # initialize empty array in constructor
    def initialize
        # Database connection establishment
        begin
            @client = Mysql2::Client.new(host: "localhost", username: "root", password: "", database: "todo_app")
            puts "Yes Connected"
            @client.query("CREATE TABLE IF NOT EXISTS todo_items(id INT PRIMARY KEY AUTO_INCREMENT,task VARCHAR(100)) ");
        rescue => e
            puts "Error while connecting to the database."
            puts e.message
        end
            # $todos = Array.new
    end

    # Main method
    def main_method
        puts "******* WELCOME TO TODO APP**********"
        get_user_choice
    end

    # Method to input user choice whether to exit or continue
    def get_user_choice
        puts "1. List All Todos"
        puts "2. Add new items to Todo"
        puts "3. Remove Item from Todo"
        puts "4. Update Item"
        puts "5. Exit"
        puts "**************************"
        puts "Choose any of the above . "
        @choice = gets.chomp.to_i
        user_choice_handle
    end

    # switch user choice
    def user_choice_handle
        case @choice
            when 1
                list_items
            when 2
                add_item
            when 3
                remove_item
            when 4
                update_item
            when 5
                puts "Good Bye!!"  
                exit
            else
                puts "Invalid Input!!Choose only from the above list."                
        end
        puts "*********************************"
        get_user_choice
    end

    # method to list all items
    def list_items
        result = @client.query("SELECT task from todo_items");
        puts "**********************"
        puts "Showing items from the todo list"
        puts "--------------------"
        puts " Tasks "
        puts "--------------------"
        begin
            result.each do |row|
                puts "| #{row['task']} |"
            end
            puts "--------------------"
        rescue Mysql::Error => e
            puts e
        end
    end

    # Add item to the list
    def add_item
        puts "Add item to add into the Todo list ==>"
        item = gets.chomp
        begin
            @client.query("INSERT INTO todo_items(task) VALUES('#{item}')")
            puts "Item added successfully to the todo list."
        rescue Mysql::Error => e
            puts e
        end
    end

    # remove item from the list
    def remove_item
        begin
            result = @client.query("SELECT task from todo_items");
            items_list = Array.new
                result.each do |row|
                    items_list.push(row['task'])
                end
        rescue Mysql::Error => e
            puts e
        end
        if items_list.empty?
            put "Todo List is empty."
        else
            puts "-------------------"
            puts "List of items."
            puts "-------------------"
            begin
                result.each do |row|
                    puts row['task']
                end
            puts "-----------------"
            rescue Mysql::Error => e
                puts e
            end
            puts "Enter the item to remove from the above list ==>"
            item_to_remove = gets.chomp
            if items_list.include?(item_to_remove)
                begin 
                    delete_query = @client.query("DELETE FROM todo_items where task='#{item_to_remove}'")
                    puts "Item Deleted"
                rescue Mysql::Error => e
                    puts e
                end
            else
                puts "Item is not in the list."
            end 
        end
    end


    # to update to do list
    def update_item
        begin
            result = @client.query("SELECT task from todo_items");
            items_list = Array.new
                result.each do |row|
                    items_list.push(row['task'])
                end
        rescue Mysql::Error => e
            puts e
        end
        unless items_list.empty?
            puts "-------------------"
            puts "List of items."
            puts "-------------------"
            begin
                result.each do |row|
                    puts row['task']
                end
                puts "--------------------"
            rescue Mysql::Error => e
                puts e
            end
            puts "Enter the item to update to the above list ==>"
            item_to_update = gets.chomp
            if items_list.include?(item_to_update)
                puts "Enter the new item name ==>"
                new_task = gets.chomp
                begin 
                    delete_query = @client.query("UPDATE todo_items set task='#{new_task}' WHERE task='#{item_to_update}' ")
                    puts "Item Updated"
                rescue Mysql::Error => e
                    puts e
                end
            else
                puts "Item is not in the list."
            end
        else
            put "Todo List is empty."
        end
    end
  
end


# Making an instance of the class and calling the main method
todoapp = TodoApplication.new
todoapp.main_method